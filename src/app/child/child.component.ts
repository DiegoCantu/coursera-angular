import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup , Validators  } from '@angular/forms';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  constructor(private form: FormBuilder) {

  }

  @Output() notifyParent: EventEmitter<any> = new EventEmitter();

  component: FormGroup;
  ngOnInit() {
    this.component = this.form.group({
      valor: new FormControl('', Validators.required)
    });
  }

  sendNotification() {
      this.notifyParent.emit(this.component.controls.valor.value);
  }
 

  
}
