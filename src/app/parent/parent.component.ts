import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
  items: String[] = [];
  ngOnInit() {
  }

  getNotification(evt: String) {
    this.items.push(evt);
  }

}
